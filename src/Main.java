import cons.Helpers;
import cons.Persons;
import service.servBin.ServiceBinaryFile;
import service.servCSV.ServiceCSV;
import service.servJSON.ServiceJSON;
import service.servXML.ServiceXML;
import service.servYAML.ServiceYAML;

import java.security.Provider;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        ServiceXML xml = new ServiceXML();
        ServiceYAML yaml = new ServiceYAML();
        ServiceJSON json = new ServiceJSON();
        ServiceCSV csv = new ServiceCSV();
        ServiceBinaryFile bin = new ServiceBinaryFile();
        Helpers helpers = new Helpers();
        ArrayList<Persons> persons = null;
        Scanner scanner = new Scanner(System.in);
        String numberFormat = null;
        int ext;

        do {
            System.out.println("    Привет, выберете номер формата с которым будете работать:");
            System.out.println("1. JSON");
            System.out.println("2. XML");
            System.out.println("3. YAML");
            System.out.println("4. CSV");
            System.out.println("5. BIN");

            numberFormat = scanner.nextLine();

            switch (numberFormat) {
                case "1":
                    persons = json.parserJson();
                    System.out.println("Вы выбрали JSON");
                    break;
                case "2":
                    persons = xml.parserXML();
                    System.out.println("Вы выбрали XML");
                    break;
                case "3":
                    persons = yaml.parserYAML();
                    System.out.println("Вы выбрали YAML");
                    break;
                case "4":
                    persons = csv.parserCSV();
                    System.out.println("Вы выбрали СSV");
                    break;
                case "5":
                    persons = bin.ParserBin();
                    System.out.println("Вы выбрали BIN");
                    break;
                default:
                    System.out.println("Неправельный ввод!");
            }
        } while (persons == null);

        do {
            ext = 0;
            System.out.println("    Выберете действие:");
            System.out.println("1. GRUD");
            System.out.println("2. Помощники");
            System.out.println("3. Выйти и сохранить");

            String numderConsole = scanner.nextLine();

            switch (numderConsole) {
                case "1":
                    System.out.println("    Команды GRUD:");
                    System.out.println("1. Создать новую запись.");
                    System.out.println("2. Получить все записи.");
                    System.out.println("3. Удалить запись по ID.");
                    System.out.println("4. Изменить запись.");
                    System.out.println("5. Назад");
                    String numberGRUD = scanner.nextLine();
                    switch (numberGRUD) {
                        case "1":
                            System.out.println("Создание новой записи.");
                            System.out.println("Веедите имя:");
                            String name = scanner.nextLine();
                            System.out.println("Введите фамилию:");
                            String lName = scanner.nextLine();
                            System.out.println("Введите возраст:");
                            String age = scanner.nextLine();
                            System.out.println("Введите город:");
                            String city = scanner.nextLine();
                            try {
                                persons.add(new Persons(createID(persons), name, lName, Integer.parseInt(age), city));
                                System.out.println("Создана запись: Person{" + name + ", " + lName + ", " + age + ", " + city + "}");
                            } catch (Exception e) {
                                System.out.println("Введите коректные данные!");
                            }
                            break;
                        case "2":
                            System.out.println("Все записи:");
                            helpers.getAll(persons);
                            break;
                        case "3":
                            System.out.println("Введите Id, для удаление: ");
                            String id = scanner.nextLine();
                            System.out.println("Удаление записи:");
                            helpers.delete(Integer.parseInt(id), persons);

                            try {
                                helpers.deleteById(Integer.parseInt(id), persons);
                            }catch (ClassCastException e){
                                e.printStackTrace();
                            }
                            catch (Exception e){
                                e.printStackTrace();
                            }

                            break;
                        case "4":
                            System.out.println("Введите Id, для редактирования: ");

                            try {
                                int id1 = Integer.parseInt(scanner.nextLine());
                                System.out.println("Редактирование  записи:");
                                Persons personsEdit = helpers.getPersonById(persons, id1);

                                System.out.println("Имя:(предыдущие данные  - " + personsEdit.getFirstName() + ")");
                                personsEdit.setFirstName(scanner.next());
                                System.out.println("Фамилия:(предыдущие данные  - " + personsEdit.getLname() + ")");
                                personsEdit.setLname(scanner.next());
                                System.out.println("Возраст:(предыдущие данные  - " + personsEdit.getLname() + ")");
                                personsEdit.setAge(Integer.parseInt(scanner.next()));
                                System.out.println("Город:(предыдущие данные  - " + personsEdit.getCity() + ")");
                                personsEdit.setCity(scanner.next());
                            } catch (NullPointerException nullPointerException) {
                                System.out.println("\nПользователь с таким id - отсутствует!\n");
                            }
                            ;
                            break;
                        case "5":
                            System.out.println("Возвращение назад!");
                        default:
                            System.out.println("Неправельный ввод!");
                    }
                    break;
                case "2":
                    System.out.println("    Команды помощники :");
                    System.out.println("1. Данные по ID");
                    System.out.println("2. Данные по городу");
                    System.out.println("3. Данные по возрасту");
                    System.out.println("4. Данные по имени");
                    System.out.println("5. Данные по фамилии");
                    System.out.println("6. Удалить всех");
                    System.out.println("7. Назад");
                    String help = scanner.nextLine();
                    switch (help) {
                        case "1":
                            System.out.println("Введите ID:");
                            String id = scanner.nextLine();
                            helpers.getById(persons, Integer.parseInt(id));
                            break;
                        case "2":
                            System.out.println("Введите город:");
                            String city = scanner.nextLine();
                            helpers.getAllBySity(persons, city);
                            break;
                        case "3":
                            System.out.println("Введите возраст:");
                            String age = scanner.nextLine();
                            helpers.getAllByAge(persons, Integer.parseInt(age));
                            break;
                        case "4":
                            System.out.println("Введите имя:");
                            String name = scanner.nextLine();
                            helpers.getAllByFirstName(persons, name);
                            break;
                        case "5":
                            System.out.println("Введите фамилию:");
                            String lname = scanner.nextLine();
                            helpers.getAllByLastName(persons, lname);
                            break;
                        case "6":
                            System.out.println("Вы уверены?");
                            System.out.println("1. Да");
                            System.out.println("2. Нет");
                            String res = scanner.nextLine();

                            if (res == "1") { //    всегда ложь  - ересь
                            //if (res.equals("1")) {
                                persons.clear();
                                helpers.getAll(persons); //- мы же собрались удалять все записи из файла
                            //    json.deleteJSON();
                                System.out.println("Удаление выполненно!");
                            } else
                                System.out.println("Удаление отмененно.");
                            break;
                        case "7":
                            System.out.println("Возвращение назад!");
                        default:
                            System.out.println("Неправельный ввод!");
                    }
                    break;
                case "3":
                    ext = 1;
                    switch (numberFormat) {
                        case "1":
                            System.out.println("Сохранения изменений JSON.");
                            json.deleteJSON();
                            json.jsonSaveAll(persons);
                            break;
                        case "2":
                            System.out.println("Сохранения изменений XML.");
                            xml.deleteXML();
                            xml.xmlSaveAll(persons);
                            break;
                        case "3":
                            System.out.println("Сохранения изменений YAML.");
                            yaml.deleteYAML();
                            yaml.yamlSaveAll(persons);
                            break;
                        case "4":
                            System.out.println("Сохранения изменений СSV.");
                            csv.deleteCSV();
                            csv.cswSaveAll(persons);
                            break;
                        case "5":
                            System.out.println("Сохранения изменений BIN.");
                            bin.deleteBin();
                            bin.saveAllBin(persons);
                        default:
                            System.out.println("Введите коректные данные!");
                    }
                    break;
                default:

            }
        } while (ext == 0);

    }

    public static int createID(ArrayList<Persons> persons) {

        char[] chars = "1234567890".toCharArray();
        StringBuilder sb = new StringBuilder(7);
        Random random = new Random();

        for (int i = 0; i < 7; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }

        String output = sb.toString();
        int id = Integer.parseInt(output);

        for (int i = 0; i < persons.size(); i++) {
            if (persons.get(i).getId() == id) {
                id = createID(persons);
            }
        }
        return id;
    }

}

package cons;

public class Persons {
    private int id;
    private String firstName;
    private String lname;
    private int age;
    private String city;

    public Persons(int id, String firstName, String lname, int age, String city) {
        this.id = id;
        this.firstName = firstName;
        this.lname = lname;
        this.age = age;
        this.city = city;
    }

    public Persons() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "Persons{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lname='" + lname + '\'' +
                ", age=" + age +
                ", city='" + city + '\'' +
                '}';
    }

    public Persons copy(Persons persons) {

        Persons newPersons = new Persons(persons.id, persons.firstName, persons.lname, persons.age, persons.city);

        return newPersons;
    }
}

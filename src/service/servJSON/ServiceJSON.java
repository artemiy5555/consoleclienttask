package service.servJSON;

import cons.Persons;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ServiceJSON {
    public static String jsonFile = "person.json";
    public static boolean emptyFile = false;

    private void writeJSON(Persons persons) {

        String oldData = parseOldBase();

        try {
            PrintWriter writer = new PrintWriter(jsonFile);
            writer.print("");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            FileWriter writer = new FileWriter(jsonFile, true);
            List<Persons> person = Arrays.asList(persons);
            for (Persons p : person) {
                writer.write(writeLine(p, oldData));
            }
            writer.flush();
            writer.close();
        } catch (IOException e) {
        }
    }

    private String parseOldBase() {

        String personsOldData = "";

        try (BufferedReader br = new BufferedReader(new FileReader(jsonFile))) {
            if ((personsOldData = br.readLine()) == null) {
                emptyFile = true;
                String newPersonsOldData = "";

                return newPersonsOldData;
            }else {emptyFile = false;}
        } catch (IOException e) {
            e.printStackTrace();
        }

        return personsOldData;
    }

    private String writeLine(Persons p, String oldData) throws IOException {

        String currentPerson = personToString(p);

        if (!emptyFile) {
            currentPerson = oldData.substring(0, oldData.length() - 1) + "," + currentPerson + "]";
        } else currentPerson = "[" + currentPerson + "]";

        return currentPerson;
    }

    private String personToString(Persons person) {
        return "{"
                + "\"id\" : \"" + person.getId() + "\", "
                + "\"firstname\" : \"" + person.getFirstName() + "\", "
                + "\"lname\" : \"" + person.getLname() + "\", "
                + "\"age\" : \"" + person.getAge() + "\", "
                + "\"city\" : \"" + person.getCity() + "\"}";
    }

    public void jsonSaveAll(ArrayList<Persons> persons) {
        for (int i = 0; i < persons.size(); i++)
            writeJSON(persons.get(i));
    }

    public ArrayList<Persons> parserJson() {

        ArrayList<Persons> person = new ArrayList<>();
        String line = "";
        String jsonSplitBy = ",";

        try (BufferedReader br = new BufferedReader(new FileReader(jsonFile))) {

            Persons persons = new Persons();
            line = br.readLine();

            if (line != null) {
                line = line.replaceAll("[\\[\\]{}\"]", "");
                String[] split = line.split(jsonSplitBy);

                for (String paramPerson : split) {

                    if (paramPerson.indexOf("id") != -1) {

                        paramPerson = toString(paramPerson);
                        persons.setId(Integer.parseInt(paramPerson));
                        continue;
                    }
                    if (paramPerson.indexOf("firstname") != -1) {
                        paramPerson = toString(paramPerson);
                        persons.setFirstName(paramPerson);
                        continue;
                    }
                    if (paramPerson.indexOf("lname") != -1) {
                        paramPerson = toString(paramPerson);
                        persons.setLname(paramPerson);
                        continue;
                    }
                    if (paramPerson.indexOf("age") != -1) {
                        paramPerson = toString(paramPerson);
                        persons.setAge(Integer.parseInt(paramPerson));
                        continue;
                    }
                    if (paramPerson.indexOf("city") != -1) {
                        paramPerson = toString(paramPerson);
                        persons.setCity(paramPerson);

                        Persons newPersons = new Persons();
                        newPersons = newPersons.copy(persons);

                        person.add(newPersons);
                    }
                }

            }

        } catch (IOException e) {}

        return person;
    }

    public String toString(String str) {

        str = str.substring(str.indexOf(":") + 1);
        str = str.trim();

        return str;
    }
    public void deleteJSON() {

        try {
            FileWriter writer = new FileWriter(jsonFile);
            writer.write("");
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

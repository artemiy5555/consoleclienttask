package service.servCSV;

import cons.Persons;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ServiceCSV {

    String csvFile = "persons.csv";

    private void writeCSV(Persons persons) {


        try {
            FileWriter writer = new FileWriter(csvFile, true);
            List<Persons> person = Arrays.asList(persons);

            for (Persons p : person) {
                List<String> list = new ArrayList<>();
                list.add(String.valueOf(p.getId()));
                list.add(p.getFirstName());
                list.add(p.getLname());
                list.add(String.valueOf(p.getAge()));
                list.add(p.getCity());
                writeLine(writer, list);
            }

            writer.flush();
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    private String followCVSformat(String value) {

        String result = value;
        if (result.contains("\"")) {
            result = result.replace("\"", "\"\"");
        }
        return result;

    }

    private void writeLine(Writer w, List<String> values) throws IOException {

        char separators = ',';
        char customQuote = ' ';
        boolean first = true;


        if (separators == ' ') {
            separators = ',';
        }

        StringBuilder sb = new StringBuilder();
        for (String value : values) {
            if (!first) {
                sb.append(separators);
            }
            if (customQuote == ' ') {
                sb.append(followCVSformat(value));
            } else {
                sb.append(customQuote).append(followCVSformat(value)).append(customQuote);
            }

            first = false;
        }
        sb.append("\n");
        w.append(sb.toString());
    }

    public void cswSaveAll(ArrayList<Persons> persons) {
        for (int i = 0; i < persons.size(); i++)
            writeCSV(persons.get(i));
    }

    public ArrayList<Persons> parserCSV() {
        String line = "";
        String cvsSplitBy = ",";
        ArrayList<Persons> person = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {

            while ((line = br.readLine()) != null) {
                String[] persons = line.split(cvsSplitBy);
                person.add(new Persons(Integer.parseInt(persons[0]), persons[1], persons[2], Integer.parseInt(persons[3]), persons[4]));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return person;
    }

    public void deleteCSV() {
        try {
            FileWriter writer = new FileWriter(csvFile);
            writer.write("");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}

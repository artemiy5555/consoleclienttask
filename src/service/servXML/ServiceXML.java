package service.servXML;

import cons.Persons;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ServiceXML {

    public static String xmlFile = "persons.xml";
    public static boolean emptyFile;

    private void writeXML(Persons persons) {

        String oldData = parseOldBase();

        try {
            PrintWriter writer = new PrintWriter(xmlFile);
            writer.print("");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            FileWriter writer = new FileWriter(xmlFile, true);
            List<Persons> person = Arrays.asList(persons);
            for (Persons p : person) {
                writer.write(writeLine(p, oldData));
            }
            writer.flush();
            writer.close();
        } catch (IOException e) {
        }
    }

    private String parseOldBase() {

        String personsOldData = "";
        String newersonsOldData = "";

        try (BufferedReader br = new BufferedReader(new FileReader(xmlFile))) {
            emptyFile = true;

            while ((personsOldData = br.readLine()) != null) {

                newersonsOldData = newersonsOldData +personsOldData+"\n";
                emptyFile = false;
            }
            return newersonsOldData;


        } catch (IOException e) {
            e.printStackTrace();
        }

        return personsOldData;
    }

    private String writeLine(Persons p, String oldData) throws IOException {

        String currentPerson = personToString(p);

        if (!emptyFile) {
            currentPerson = oldData.substring(0, oldData.length() - 11) + currentPerson+"\n</Persons>";
        } else currentPerson = "<Persons>\n" + currentPerson + "\n</Persons>";

        return currentPerson;
    }

    private String personToString(Persons person) {
        return "<Person>"
                + "\n<id>" + person.getId() + "</id>"
                + "\n<firstname>" + person.getFirstName() + "</firstname>"
                + "\n<lname>" + person.getLname() + "</lname>"
                + "\n<age>" + person.getAge() + "</age>"
                + "\n<city>" + person.getCity() + "</city>"
                + "\n</Person>";
    }
    public void xmlSaveAll(ArrayList<Persons> persons) {
        for (int i = 0; i < persons.size(); i++)
            writeXML(persons.get(i));
    }

    public ArrayList<Persons> parserXML() {

        ArrayList<Persons> person = new ArrayList<>();
        String line = "";
        String XMLSplitBy = ": ";
        String split = "";
        try (BufferedReader br = new BufferedReader(new FileReader(xmlFile))) {
            Persons persons = new Persons();
            while ((line = br.readLine()) != null) {

                try {
                    split = line.substring(line.indexOf(">") + 1, line.lastIndexOf("<"));
                } catch (Exception e) {
                }

                if (line.indexOf("id") != -1)
                    persons.setId(Integer.parseInt(split));
                if (line.indexOf("firstname") != -1)
                    persons.setFirstName(split);
                if (line.indexOf("lname") != -1)
                    persons.setLname(split);
                if (line.indexOf("age") != -1)
                    persons.setAge(Integer.parseInt(split));
                if (line.indexOf("city") != -1) {
                    persons.setCity(split);
                    Persons newPersons = new Persons();
                    newPersons = newPersons.copy(persons);
                    person.add(newPersons);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return person;
    }

    public void deleteXML(){

        try {
            FileWriter writer = new FileWriter(xmlFile);
            writer.write("");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

package service.servBin;

import cons.Persons;
import org.junit.Test;
import service.servJSON.ServiceJSON;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ServiceBinaryFileTest {

    @Test
    public void classServiceBin_Available() {

        ServiceBinaryFile serviceBinaryFile;

        serviceBinaryFile = new ServiceBinaryFile();

        assertNotNull(serviceBinaryFile);
    }

    @Test
    public void test_service_saveAllBinAndParse_void() {
        ServiceBinaryFile serviceBinaryFile = new ServiceBinaryFile();
        serviceBinaryFile.deleteBin();

        ArrayList<Persons> expectedPersons = new ArrayList<>();
        expectedPersons.add(new Persons(1,"Artemiy","Brook",20,"Kiev"));
        expectedPersons.add(new Persons(1,"Vasia","Cherii",19,"Kiev"));

        serviceBinaryFile.saveAllBin(expectedPersons);

        ArrayList<Persons> actualPersons = serviceBinaryFile.ParserBin();
        assertEquals(expectedPersons.toString(), actualPersons.toString());
        serviceBinaryFile.deleteBin();
    }

    @Test
    public void test_service_deleteBin_void() {

        ServiceBinaryFile serviceBinaryFile = new ServiceBinaryFile();
        serviceBinaryFile.deleteBin();

        String str ="";
        try(FileInputStream fin=new FileInputStream("person.bin"))
        {
            int i=-1;
            while((i=fin.read())!=-1){

                str+=(char)i;
            }
        }
        catch(IOException ex){
            System.out.println(ex.getMessage());
        }
        assertEquals("",str);
    }

}

package service.servYAML;

import cons.Persons;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class ServiceYamlTest {

    @Test
    public void test_service_YamlSeveAllAndParserYaml(){

        ServiceYAML serviceYAML= new ServiceYAML();
        serviceYAML.deleteYAML();

        ArrayList<Persons> expectedPersons = new ArrayList<>();
        expectedPersons.add(new Persons(1,"Danil","Chooseh",20,"Kiev"));
        expectedPersons.add(new Persons(1,"Bodya","God",19,"Lviv"));

        serviceYAML.yamlSaveAll(expectedPersons);

        ArrayList<Persons> actualPersons = serviceYAML.parserYAML();
        assertEquals(expectedPersons.toString(), actualPersons.toString());
        serviceYAML.deleteYAML();
    }


    @Test
    public void test_service_deleteYAML(){

        ServiceYAML serviceYAML = new ServiceYAML();
        serviceYAML.deleteYAML();

        String str ="";
        try(FileInputStream fin=new FileInputStream("person.yaml"))
        {
            int i=-1;
            while((i=fin.read())!=-1){

                str+=(char)i;
            }
        }
        catch(IOException ex){
            System.out.println(ex.getMessage());
        }
        assertEquals("",str);
    }


}

package cons;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.util.ArrayList;

import static org.junit.Assert.assertNotNull;


public class HelpersTest {

    private ByteArrayOutputStream output = new ByteArrayOutputStream();

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(output));
    }

    @Test
    public void classHelpers_Available() {

        Helpers helpers;

        helpers = new Helpers();

        assertNotNull(helpers);
    }

    @Test
    public void getAll_arrayListPerson_printPersonFromDB() {
        Helpers helpers = new Helpers();
        ArrayList<Persons> persons= new ArrayList<>();
        persons.add(new Persons(2105, "Вадим", "Горишний", 30, "Ирпень"));
        persons.add(new Persons(2106, "Иван", "Заморский", 30, "Будапешт"));

        helpers.getAll(persons);

        Assert.assertEquals("Persons{id=2105, firstName='Вадим', lname='Горишний', age=30, city='Ирпень'}"+"\n"
                +"Persons{id=2106, firstName='Иван', lname='Заморский', age=30, city='Будапешт'}"+"\n", output.toString());
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
    }

}
